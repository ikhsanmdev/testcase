import 'package:bloc/bloc.dart';
import 'package:email_validator/email_validator.dart';
import 'package:majootestcase/data/models/user.dart';
import 'package:majootestcase/data/repository/user_local_data_source.dart';
import 'package:majootestcase/data/repository/user_prefs_data_source.dart';
import 'package:meta/meta.dart';

part 'registration_event.dart';

part 'registration_state.dart';

class RegistrationBloc extends Bloc<RegistrationEvent, RegistrationState> {
  final UserLocalDataSource userLocalDataSource;
  final UserPrefsDataSource userPrefsDataSource;

  RegistrationBloc(this.userLocalDataSource, this.userPrefsDataSource)
      : super(RegistrationInitial()) {
    on<RegistrationEvent>((event, emit) async {
      if (event is RegisterNow) {
        final username = event.username;
        final email = event.email;
        final password = event.password;

        if (username.isEmpty || email.isEmpty || password.isEmpty) {
          emit(RegistrationError(
              "Form tidak boleh kosong, mohon cek kembali data yang anda inputkan"));
          return;
        } else if (!EmailValidator.validate(email)) {
          emit(RegistrationError("Masukkan email yang valid"));
          return;
        }

        emit(RegistrationLoading());
        final user = User(email: email, userName: username, password: password);
        try {
          final result = await userLocalDataSource.createUser(user);
          print(result);
          if (result > 0) {
            await userPrefsDataSource.setUserLoggedIn(user);
            emit(RegistrationSuccess());
          } else {
            emit(RegistrationError("Tidak dapat mendaftarkan akun"));
          }
        } catch (e) {
          emit(RegistrationError("Tidak dapat mendaftarkan akun"));
        }
      }
    });
  }
}
