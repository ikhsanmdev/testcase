part of 'registration_bloc.dart';

@immutable
abstract class RegistrationEvent {}

class RegisterNow extends RegistrationEvent {
  final String username;
  final String email;
  final String password;

  RegisterNow(this.username, this.email, this.password);
}