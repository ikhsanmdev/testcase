part of 'home_bloc.dart';

@immutable
abstract class HomeEvent {}

class LoadMovie extends HomeEvent {}

class Logout extends HomeEvent {}