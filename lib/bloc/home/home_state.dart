part of 'home_bloc.dart';

@immutable
abstract class HomeState {}

class HomeInitial extends HomeState {}

class HomeLoading extends HomeState {}

class HomeSuccess extends HomeState {
  final List<Data> source;

  HomeSuccess(this.source);
}

class HomeError extends HomeState {
  final String message;

  HomeError(this.message);
}

class HomeLoggedOut extends HomeState {}
