import 'package:bloc/bloc.dart';
import 'package:majootestcase/data/models/movie_response.dart';
import 'package:majootestcase/data/repository/movie_remote_data_source.dart';
import 'package:majootestcase/data/repository/user_prefs_data_source.dart';
import 'package:meta/meta.dart';

part 'home_event.dart';

part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final MovieRemoteDataSource movieRemoteDataSource;
  final UserPrefsDataSource prefsDataSource;

  HomeBloc(this.movieRemoteDataSource, this.prefsDataSource)
      : super(HomeInitial()) {
    on<HomeEvent>((event, emit) async {
      if (event is LoadMovie) {
        emit(HomeLoading());
        try {
          final response = await movieRemoteDataSource.getMovies();
          emit(HomeSuccess(response.data));
        } catch (e) {
          emit(HomeError(e as String));
        }
      } else if (event is Logout) {
        await prefsDataSource.setUserLoggedOut();
        emit(HomeLoggedOut());
      }
    });
  }
}
