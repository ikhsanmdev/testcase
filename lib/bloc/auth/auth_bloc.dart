import 'package:bloc/bloc.dart';
import 'package:email_validator/email_validator.dart';
import 'package:majootestcase/data/repository/user_local_data_source.dart';
import 'package:majootestcase/data/repository/user_prefs_data_source.dart';
import 'package:meta/meta.dart';

part 'auth_event.dart';

part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final UserLocalDataSource userLocalDataSource;
  final UserPrefsDataSource userPrefsDataSource;

  AuthBloc(this.userLocalDataSource, this.userPrefsDataSource)
      : super(AuthInitial()) {
    on<AuthEvent>((event, emit) async {
      if (event is CheckLastLogin) {
        final user = userPrefsDataSource.getUserLoggedIn();
        if (user != null) {
          emit(AuthSuccess());
        }
      } else if (event is LoginNow) {
        final email = event.email;
        final password = event.password;

        if (email.isEmpty || password.isEmpty) {
          emit(AuthError(
              "Form tidak boleh kosong, mohon cek kembali data yang anda inputkan"));
          return;
        } else if (!EmailValidator.validate(email)) {
          emit(AuthError("Masukkan email yang valid"));
          return;
        }

        emit(AuthLoading());
        final user = await userLocalDataSource.getUser(email, password);
        if (user != null) {
          await userPrefsDataSource.setUserLoggedIn(user);
          emit(AuthSuccess());
        } else {
          emit(AuthError("Login gagal , periksa kembali inputan anda"));
        }
      }
    });
  }
}
