part of 'auth_bloc.dart';

@immutable
abstract class AuthEvent {}

class CheckLastLogin extends AuthEvent {}

class LoginNow extends AuthEvent {
  final String email;
  final String password;

  LoginNow(this.email, this.password);
}