import 'package:flutter/material.dart';

class PrimaryButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String text;
  final EdgeInsets padding;
  final bool isEnabled;
  final Color color;
  final Color textColor;
  final bool isLoading;

  const PrimaryButton({
    Key? key,
    required this.onPressed,
    required this.text,
    this.padding = const EdgeInsets.symmetric(horizontal: 20),
    this.isEnabled = true,
    this.color = Colors.blue,
    this.textColor = Colors.white,
    this.isLoading = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (isLoading) {
      return const SizedBox(
        height: 40,
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: Center(
            child: CircularProgressIndicator(),
          ),
        ),
      );
    }
    return Container(
      margin: padding,
      width: double.infinity,
      height: 40,
      child: TextButton(
        onPressed: isEnabled ? onPressed : null,
        style: ButtonStyle(
          shape: MaterialStateProperty.all(const StadiumBorder()),
          backgroundColor: MaterialStateProperty.resolveWith((states) {
            if (states.contains(MaterialState.disabled)) {
              return Colors.white24;
            }
            if (states.contains(MaterialState.focused)) {
              return Colors.deepOrange;
            }
            if (states.contains(MaterialState.hovered)) {
              return Colors.orange;
            }
            return color;
          }),
          foregroundColor: MaterialStateProperty.resolveWith((states) {
            if (states.contains(MaterialState.disabled)) {
              return Colors.white;
            }
            return textColor;
          }),
        ),
        child: Text(
          text,
          style: Theme.of(context).textTheme.button?.copyWith(
                fontWeight: FontWeight.bold,
                color: isEnabled ? textColor : Colors.white,
                letterSpacing: 1.0,
              ),
        ),
      ),
    );
  }
}
