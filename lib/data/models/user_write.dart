class UserWrite {
  String email;
  String password;

  UserWrite({
    required this.email,
    required this.password,
  });

  UserWrite.fromJson(Map<String, dynamic> json)
      : email = json['email'],
        password = json['password'];

  Map<String, dynamic> toJson() => {'email': email, 'password': password};
}
