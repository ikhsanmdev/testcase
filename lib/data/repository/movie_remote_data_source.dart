import 'package:dio/dio.dart';
import 'package:majootestcase/data/models/movie_response.dart';

abstract class MovieRemoteDataSource {
  Future<MovieResponse> getMovies();
}

class MovieRemoteDataSourceImpl extends MovieRemoteDataSource {
  final Dio dio;

  MovieRemoteDataSourceImpl(this.dio);

  @override
  Future<MovieResponse> getMovies() async {
    try {
      final response = await dio.get("/auto-complete?q=game%20of%20thr");
      return MovieResponse.fromJson(response.data);
    } catch (e) {
      print(e);
      return Future.error("Server tidak bisa diakses. Silakan coba lagi");
    }
  }
}
