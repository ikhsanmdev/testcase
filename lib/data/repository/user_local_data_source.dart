import 'package:majootestcase/data/models/user.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:sqflite/sqflite.dart';

abstract class UserLocalDataSource {
  Future<int> createUser(User user);

  Future<User?> getUser(String email, String password);
}

class UserLocalDataSourceImpl extends UserLocalDataSource {
  final Database database;

  UserLocalDataSourceImpl(this.database);

  @override
  Future<int> createUser(User user) async {
    return await database.insert(TABLE_USER, user.toJson());
  }

  @override
  Future<User?> getUser(String email, String password) async {
    final result = await database.query(
      TABLE_USER,
      where: 'email = ? AND password = ?',
      whereArgs: [email, password],
    );
    if (result.isEmpty) {
      return null;
    }
    
    print(result);
    return User.fromJson(result.first);
  }
}
