import 'dart:convert';

import 'package:majootestcase/data/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class UserPrefsDataSource {
  Future<bool> setUserLoggedIn(User user);
  User? getUserLoggedIn();
  Future<bool> setUserLoggedOut();
}

class UserPrefsDataSourceImpl extends UserPrefsDataSource{
  final SharedPreferences sharedPreferences;

  UserPrefsDataSourceImpl(this.sharedPreferences);
  
  @override
  Future<bool> setUserLoggedIn(User user) {
    return sharedPreferences.setString('user', jsonEncode(user.toJson()));
  }
  
  @override
  User? getUserLoggedIn() {
    if (sharedPreferences.containsKey('user')) {
      final result = sharedPreferences.getString('user');
      return User.fromJson(jsonDecode(result!));
    }
    return null;
  }

  @override
  Future<bool> setUserLoggedOut() {
    return sharedPreferences.remove('user');
  }
  
}