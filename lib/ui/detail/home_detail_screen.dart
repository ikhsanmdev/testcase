import 'package:flutter/material.dart';
import 'package:majootestcase/data/models/movie_response.dart';

class HomeDetailScreen extends StatelessWidget {
  final Data data;

  const HomeDetailScreen({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(data.l),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(16),
              child: Row(
                children: [
                  SizedBox(
                    height: 200,
                    width: 150,
                    child: Image.network(data.i.imageUrl),
                  ),
                  Flexible(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            data.l,
                            style: Theme.of(context).textTheme.headline5,
                          ),
                          Text(
                            data.year.toString(),
                            style:
                                Theme.of(context).textTheme.bodyText1?.copyWith(
                                      fontSize: 15,
                                    ),
                          ),
                          Visibility(
                            visible: data.s != null,
                            child: const SizedBox(
                              height: 10,
                            ),
                          ),
                          Visibility(
                            visible: data.s != null,
                            child: Text(
                              data.s ?? "",
                              style: Theme.of(context).textTheme.bodyText2,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Visibility(
              visible: data.series != null,
              child: SizedBox(
                height: 230,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: data.series?.length,
                  itemBuilder: (context, position) {
                    return SizedBox(
                      width: 200,
                      child: Card(
                        child: Column(
                          children: [
                            SizedBox(
                              height: 150,
                              child: Image.network(
                                data.series?[position].i.imageUrl ?? "",
                                fit: BoxFit.cover,
                              ),
                            ),
                            Flexible(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(data.series?[position].l ?? ""),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
