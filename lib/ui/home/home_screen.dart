import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/home/home_bloc.dart';
import 'package:majootestcase/common/widget/primary_button.dart';
import 'package:majootestcase/data/models/movie_response.dart';
import 'package:majootestcase/data/repository/user_prefs_data_source.dart';
import 'package:majootestcase/injection_container.dart';
import 'package:majootestcase/utils/routes.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => sl<HomeBloc>()..add(LoadMovie()),
      child: Builder(builder: (context) {
        return Scaffold(
          appBar: AppBar(
            title: Text(
              "Halo, ${sl<UserPrefsDataSource>().getUserLoggedIn()?.userName ?? ""}",
            ),
            actions: [
              IconButton(
                onPressed: () {
                  context.read<HomeBloc>().add(Logout());
                },
                icon: Icon(Icons.logout),
              ),
            ],
          ),
          body: BlocConsumer<HomeBloc, HomeState>(
            listener: (context, state) {
              if (state is HomeLoggedOut) {
                Navigator.pushNamedAndRemoveUntil(
                  context,
                  Routes.login,
                  (route) => false,
                );
              }
            },
            builder: (context, state) {
              if (state is HomeSuccess) {
                return ListView.builder(
                  itemCount: state.source.length,
                  itemBuilder: (context, index) {
                    final item = state.source[index];
                    return InkWell(
                      child: HomeItem(data: item),
                      onTap: () {
                        Navigator.pushNamed(
                          context,
                          Routes.detail,
                          arguments: item,
                        );
                      },
                    );
                  },
                );
              } else if (state is HomeError) {
                return HomeErrorBox(message: state.message);
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            },
          ),
        );
      }),
    );
  }
}

class HomeItem extends StatelessWidget {
  final Data data;

  const HomeItem({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.symmetric(
        vertical: 8,
        horizontal: 16,
      ),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(10),
        ),
      ),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(10),
            child: Image.network(data.i.imageUrl),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 8,
              horizontal: 20,
            ),
            child: Text(
              data.l,
              textDirection: TextDirection.ltr,
            ),
          )
        ],
      ),
    );
  }
}

class HomeErrorBox extends StatelessWidget {
  final String message;

  const HomeErrorBox({
    Key? key,
    required this.message,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(
            Icons.signal_wifi_connected_no_internet_4,
            size: 60,
          ),
          Padding(
            padding: const EdgeInsets.all(20),
            child: Text(message),
          ),
          PrimaryButton(
            onPressed: () {
              context.read<HomeBloc>().add(LoadMovie());
            },
            text: 'Refresh',
          ),
        ],
      ),
    );
  }
}
