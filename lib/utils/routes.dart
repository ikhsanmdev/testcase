import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/utils/route_handlers.dart';

class Routes {
  static String login = "/";
  static String registration = "/registration";
  static String home = "/home";
  static String detail = "/detail";
  
  static void configureRoutes(FluroRouter router) {
    router.notFoundHandler = Handler(
      handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
        debugPrint("Route unknown.");
        Future.microtask(() {
          Navigator.pushNamed(context!, login);
        });
        return;
      });
    
    const transitionType = TransitionType.material;
    router.define(login, handler: loginHandler, transitionType: transitionType);
    router.define(registration,
      handler: registrationHandler, transitionType: transitionType);
    router.define(home, handler: homeHandler, transitionType: transitionType);
    router.define(
      detail, handler: detailHandler, transitionType: transitionType);
  }
}
