class Preference {
  static const USER_INFO = "user-info";
}

class Api {
  static const BASE_URL = "";
  static const LOGIN = "/login";
  static const REGISTER = "/register";
}

class Font {
}

class ScreenUtilConstants {
  static const width = 320.0;
  static const height = 640.0;
}

// database constant
const DATABASE_NAME = "testcase.db";
const CREATE_TABLE_USER_QUERY = "CREATE TABLE $TABLE_USER(email TEXT PRIMARY KEY, username TEXT, password TEXT)";
const TABLE_USER = 'user';

// remote constant
const BASE_URL = "https://imdb8.p.rapidapi.com";
const HEADERS = {
  "x-rapidapi-key": "7964f9b40cmsh50b27828811ef72p188181jsnfcf3acc5884d",
  "x-rapidapi-host": "imdb8.p.rapidapi.com"
};