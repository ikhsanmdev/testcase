import 'package:fluro/fluro.dart';
import 'package:flutter/cupertino.dart';
import 'package:majootestcase/data/models/movie_response.dart';
import 'package:majootestcase/ui/detail/home_detail_screen.dart';
import 'package:majootestcase/ui/home/home_screen.dart';
import 'package:majootestcase/ui/login/login_screen.dart';
import 'package:majootestcase/ui/registration/registration_screen.dart';

final loginHandler = Handler(handlerFunc: (context, params) {
  return const LoginScreen();
});

final registrationHandler = Handler(handlerFunc: (context, params) {
  return const RegistrationScreen();
});

final homeHandler = Handler(handlerFunc: (context, params) {
  return const HomeScreen();
});

final detailHandler = Handler(handlerFunc: (context, params) {
  final data = ModalRoute.of(context!)?.settings.arguments as Data;
  return HomeDetailScreen(data: data);
});
