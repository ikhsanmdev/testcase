import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:majootestcase/bloc/auth/auth_bloc.dart';
import 'package:majootestcase/bloc/home/home_bloc.dart';
import 'package:majootestcase/bloc/registration/registration_bloc.dart';
import 'package:majootestcase/data/repository/movie_remote_data_source.dart';
import 'package:majootestcase/data/repository/user_local_data_source.dart';
import 'package:majootestcase/data/repository/user_prefs_data_source.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:path/path.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';

final sl = GetIt.instance;

Future<void> init() async {
  // register all bloc
  sl.registerFactory(() => AuthBloc(sl(), sl()));
  sl.registerFactory(() => RegistrationBloc(sl(), sl()));
  sl.registerFactory(() => HomeBloc(sl(), sl()));

  // register data source
  sl.registerLazySingleton<UserLocalDataSource>(
    () => UserLocalDataSourceImpl(sl()),
  );
  sl.registerLazySingleton<UserPrefsDataSource>(
    () => UserPrefsDataSourceImpl(sl()),
  );
  sl.registerLazySingleton<MovieRemoteDataSource>(
    () => MovieRemoteDataSourceImpl(sl()),
  );

  // register database
  final database = await _createDatabase();
  sl.registerLazySingleton(() => database);
  
  // register shared preferences
  final sharedPrefs = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPrefs);
  
  // register dio
  sl.registerLazySingleton(() {
    final options = BaseOptions(
      baseUrl: BASE_URL,
      connectTimeout: 12000,
      receiveTimeout: 12000,
      headers: HEADERS,
    );

    final logger = PrettyDioLogger(
      requestHeader: true,
      requestBody: true,
      responseBody: true,
      responseHeader: false,
      error: true,
      compact: true,
      maxWidth: 200,
    );

    return Dio(options)..interceptors.add(logger);
  });
}

Future<Database> _createDatabase() async {
  return openDatabase(
    // Set the path to the database. Note: Using the `join` function from the
    // `path` package is best practice to ensure the path is correctly
    // constructed for each platform.
    join(await getDatabasesPath(), DATABASE_NAME),
    // When the database is first created, create a table to store dogs.
    onCreate: (db, version) {
      // Run the CREATE TABLE statement on the database.
      return db.execute(CREATE_TABLE_USER_QUERY);
    },
    // Set the version. This executes the onCreate function and provides a
    // path to perform database upgrades and downgrades.
    version: 1,
  );
}
