import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/injection_container.dart';
import 'package:majootestcase/utils/routes.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await init();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp();

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late FluroRouter router;

  _MyAppState() {
    router = FluroRouter();
    Routes.configureRoutes(router);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: Routes.login,
      onGenerateRoute: router.generator,
    );
  }
}

// class MyHomePageScreen extends StatelessWidget {
//   const MyHomePageScreen({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return BlocBuilder<AuthBlocCubit, AuthBlocState>(
//       builder: (context, state) {
//         if (state is AuthBlocLoginState) {
//           return LoginPage();
//         } else if (state is AuthBlocLoggedInState) {
//           return BlocProvider(
//             create: (context) => HomeBlocCubit()..fetching_data(),
//             child: HomeBlocScreen(),
//           );
//         }
//
//         return Center(
//           child: Text(kDebugMode ? "state not implemented $state" : ""),
//         );
//       },
//     );
//   }
// }
